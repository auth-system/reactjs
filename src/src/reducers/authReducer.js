import { AUTH_RESET, AUTH_UPDATE } from '../actions/types'

const initialState = {
	isAuth: null,
	token: null,
	username: null,
}

const authReducer = (state = initialState, action) => {
	switch (action.type) {
		case AUTH_RESET:
			return { ...initialState }
		case AUTH_UPDATE:
			return {
				...state,
				...action.payload,
			}
		default:
			return state
	}
}

export default authReducer
