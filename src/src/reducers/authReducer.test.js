import { AUTH_RESET, AUTH_UPDATE } from '../actions/types'
import authReducer from './authReducer'

const initialState = {
	isAuth: null,
	token: null,
	username: null,
}

describe('Auth Reducer', () => {
	it('Default State', () =>
		expect(authReducer(undefined, { type: 'DEFAULT' })).toEqual(initialState))

	it('Reset State', () =>
		expect(authReducer(undefined, { type: AUTH_RESET })).toEqual(initialState))

	it('Update State', () => {
		const payload = {
			isAuth: true,
			token: 'asdekaofnasuivyaosigvbasiubvasdv',
			username: 'admin',
		}
		expect(authReducer(undefined, { type: AUTH_UPDATE, payload })).toEqual(
			payload
		)
	})
})
