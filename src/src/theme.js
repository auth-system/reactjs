import { makeStyles } from '@material-ui/core'

export const useStyles = makeStyles(theme => ({
	centerizer: {
		alignItems: 'center',
		display: 'flex',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		textAlign: 'center',
	},
	formErrorMessage: {
		color: theme.palette.error.main,
		fontSize: theme.typography.body2.fontSize,
		marginLeft: theme.spacing(1),
		marginTop: theme.spacing(1),
		textAlign: 'left',
	},
	link: {
		color: 'inherit',
		textDecoration: 'inherit',
	},
	root: {
		borderRadius: '0px',
		display: 'flex',
		flex: 1,
	},
}))
