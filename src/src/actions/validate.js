const PATT_ALPHANUMERIC = /^[a-zA-Z0-9 ]*$/
const PATT_EMAIL = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]+$/

const validate = (name, data, constraints) => {
	const value = data[name].value

	name = name
		.split('_')
		.map(part => part[0].toUpperCase() + part.slice(1))
		.join(' ')

	if (constraints.presence && !value) return `${name} cannot be empty.`

	if (constraints.alphanumeric && !PATT_ALPHANUMERIC.exec(value))
		return `${name} can contain only alphabets and numbers.`

	if (constraints.email && !PATT_EMAIL.exec(value))
		return 'Please enter valid email address.'

	if (constraints.equality && data[constraints.equality.field].value !== value)
		return constraints.equality.message

	if (constraints.format)
		for (const item of constraints.format)
			if (!item.pattern.exec(value)) return item.message

	return ''
}

export default validate
