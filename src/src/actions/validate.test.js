import validate from './validate'

describe('Form Validation', () => {
	it('Presence', () => {
		const data = { field: { value: '' } }
		const formConstraints = { presence: true }
		const name = 'field'

		expect(validate(name, data, formConstraints)).toBe('Field cannot be empty.')
	})

	it('Alphanumeric', () => {
		const data = { field: { value: '#' } }
		const formConstraints = { alphanumeric: true }
		const name = 'field'

		expect(validate(name, data, formConstraints)).toBe(
			'Field can contain only alphabets and numbers.'
		)
	})

	it('Email', () => {
		const data = { field: { value: '#' } }
		const formConstraints = { email: true }
		const name = 'field'

		expect(validate(name, data, formConstraints)).toBe(
			'Please enter valid email address.'
		)
	})

	it('Equality', () => {
		const message = 'Both fields should be equal'

		const data = { field: { value: '#' }, field2: { value: '@' } }
		const formConstraints = { equality: { field: 'field2', message } }
		const name = 'field'

		expect(validate(name, data, formConstraints)).toBe(message)
	})

	it('Format', () => {
		const message = 'Field should contain only lowercase alphabets.'
		const message2 = 'Field should contain only a or b.'

		const data = { field: { value: 'abC' } }
		const data2 = { field: { value: 'abc' } }
		const formConstraints = {
			format: [
				{ pattern: /^[a-z]*$/, message },
				{ pattern: /^[a-b]*$/, message: message2 },
			],
		}
		const name = 'field'

		expect(validate(name, data, formConstraints)).toBe(message)
		expect(validate(name, data2, formConstraints)).toBe(message2)
	})

	it('Valid Data', () => {
		const data = { field: { value: 'value' } }
		const formConstraints = { presence: true }
		const name = 'field'

		expect(validate(name, data, formConstraints)).toBe('')
	})
})
