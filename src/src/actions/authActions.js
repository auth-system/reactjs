import { AUTH_RESET, AUTH_UPDATE } from './types'

export const authReset = () => async dispatch => dispatch({ type: AUTH_RESET })

export const authUpdate = data => async dispatch =>
	dispatch({ type: AUTH_UPDATE, payload: data })
