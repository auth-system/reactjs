import store from '../store'
import { authReset, authUpdate } from './authActions'
import { AUTH_RESET, AUTH_UPDATE } from './types'

it('Auth Reset', () => {
	store.dispatch(authReset()).then(params => {
		expect(params).toEqual({ type: AUTH_RESET })
	})
})

it('Auth Update', () => {
	const payload = {
		isAuth: true,
		token: 'asdekaofnasuivyaosigvbasiubvasdv',
		username: 'admin',
	}

	store.dispatch(authUpdate(payload)).then(params => {
		expect(params).toEqual({ type: AUTH_UPDATE, payload })
	})
})
