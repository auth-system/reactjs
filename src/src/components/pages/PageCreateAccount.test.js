import React from 'react'
import { mount } from 'enzyme'
import { BrowserRouter as Router } from 'react-router-dom'

import PageCreateAccount from './PageCreateAccount'

const component = mount(
	<Router>
		<PageCreateAccount history={[]} />
	</Router>
)

describe('Page: Login', () => {
	it('Default', () => {
		expect(component).toMatchSnapshot()
	})

	it('Invalid username', () => {
		const event = { target: { name: 'username', value: 'admin#' } }
		component.find('input[name="username"]').simulate('change', event)
		expect(component).toMatchSnapshot()
	})

	it('Empty username', () => {
		const event = { target: { name: 'username', value: '' } }
		component.find('input[name="username"]').simulate('change', event)
		expect(component).toMatchSnapshot()
	})

	it('Short password', () => {
		let event = { target: { name: 'username', value: 'admin' } }
		component.find('input[name="username"]').simulate('change', event)

		event = { target: { name: 'password', value: 'admin' } }
		component.find('input[name="password"]').simulate('change', event)

		expect(component).toMatchSnapshot()
	})

	it('Empty password', () => {
		const event = { target: { name: 'password', value: '' } }
		component.find('input[name="password"]').simulate('change', event)

		expect(component).toMatchSnapshot()
	})

	it('Password with no lowercase', () => {
		const event = { target: { name: 'password', value: 'PASSWORD' } }
		component.find('input[name="password"]').simulate('change', event)

		expect(component).toMatchSnapshot()
	})

	it('Password with no uppercase', () => {
		const event = { target: { name: 'password', value: 'password' } }
		component.find('input[name="password"]').simulate('change', event)

		expect(component).toMatchSnapshot()
	})

	it('Password with no number', () => {
		const event = { target: { name: 'password', value: 'Password' } }
		component.find('input[name="password"]').simulate('change', event)

		expect(component).toMatchSnapshot()
	})

	it('Password with no special character', () => {
		const event = { target: { name: 'password', value: 'Passw0rd' } }
		component.find('input[name="password"]').simulate('change', event)

		expect(component).toMatchSnapshot()
	})

	it('Passwords do not match', () => {
		let event = { target: { name: 'password', value: 'P@ssw0rd' } }
		component.find('input[name="password"]').simulate('change', event)

		event = { target: { name: 'confirm_password', value: 'Password' } }
		component.find('input[name="confirm_password"]').simulate('change', event)

		expect(component).toMatchSnapshot()
	})

	it('Submit with error', () => {
		component.find('form').simulate('submit')
		expect(component).toMatchSnapshot()
	})

	it('Submit', () => {
		const event = { target: { name: 'confirm_password', value: 'P@ssw0rd' } }
		component.find('input[name="confirm_password"]').simulate('change', event)
		component.find('form').simulate('submit')
		expect(component).toMatchSnapshot()
	})
})
