import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'

import PageLogin from './PageLogin'
import store from '../../store'

const component = mount(
	<Provider store={store}>
		<Router>
			<PageLogin history={[]} />
		</Router>
	</Provider>
)

describe('Page: Login', () => {
	it('Default', () => {
		expect(component).toMatchSnapshot()
	})

	it('Invalid username', () => {
		const event = { target: { name: 'username', value: 'admin#' } }
		component.find('input[name="username"]').simulate('change', event)
		expect(component).toMatchSnapshot()
	})

	it('Empty', () => {
		const event = { target: { name: 'username', value: '' } }
		component.find('input[name="username"]').simulate('change', event)
		expect(component).toMatchSnapshot()
	})

	it('Valid username', () => {
		const event = { target: { name: 'username', value: 'admin' } }
		component.find('input[name="username"]').simulate('change', event)
		expect(component).toMatchSnapshot()
	})

	it('Submit with password empty', () => {
		component.find('form').simulate('submit')
		expect(component).toMatchSnapshot()
	})

	it('Submit', () => {
		const event = { target: { name: 'password', value: 'admin' } }
		component.find('input[name="password"]').simulate('change', event)
		component.find('form').simulate('submit')
		expect(component).toMatchSnapshot()
	})
})
