import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'

import PageLogout from './PageLogout'
import store from '../../store'

test('Logout page', () => {
	const component = mount(
		<Provider store={store}>
			<Router>
				<PageLogout />
			</Router>
		</Provider>
	)

	expect(component).toMatchSnapshot()
})
