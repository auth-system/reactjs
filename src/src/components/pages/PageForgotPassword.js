import React, { useState } from 'react'
import {
	Button,
	Grid,
	makeStyles,
	Paper,
	TextField,
	Typography,
} from '@material-ui/core'
import { Link } from 'react-router-dom'

import logo from '../../logo.svg'
import validate from '../../actions/validate'
import { useStyles } from '../../theme'

const formConfiguration = {
	email_address: {
		email: true,
		presence: true,
	},
}

const useStylesLocal = makeStyles(theme => ({
	inputField: {
		width: '100%',
	},
	formBox: {
		margin: theme.spacing(3),
		maxWidth: '360px',
		padding: theme.spacing(3),
	},
	logo: {
		height: '60px',
		marginBottom: '24px',
	},
}))

const PageForgotPassword = () => {
	const [formData, setFormData] = useState({
		email_address: {
			error: '',
			value: '',
		},
	})
	const [success, setSuccess] = useState(false)

	const classes = useStyles()
	const classesLocal = useStylesLocal()

	const inputHandler = ({ target: { name, value } }) => {
		const copy = { ...formData }
		copy[name].value = value
		copy[name].error = validate(name, copy, formConfiguration[name])
		setFormData(copy)
	}

	const submitResetEmail = event => {
		event.preventDefault()

		const copy = { ...formData }
		const errors = []

		for (const key in formData) {
			copy[key].error = validate(key, copy, formConfiguration[key])
			if (copy[key].error) errors.push(copy[key].error)
		}

		setFormData(copy)

		if (errors.length === 0) setSuccess(true)
	}

	return (
		<div className={`${classes.centerizer} ${classes.root}`}>
			<Paper className={classesLocal.formBox} elevation={3}>
				<img alt='logo' className={classesLocal.logo} src={logo} />
				{!success && (
					<form autoComplete='off' noValidate onSubmit={submitResetEmail}>
						<Grid container spacing={3}>
							<Grid item xs={12}>
								<TextField
									autoFocus={true}
									className={classesLocal.inputField}
									label='User Email Address'
									name='email_address'
									onChange={inputHandler}
									value={formData.email_address.value}
									variant='outlined'
								/>
								<Typography className={classes.formErrorMessage}>
									{formData.email_address.error}
								</Typography>
							</Grid>
							<Grid item xs={12} sm={8}>
								<Button
									color='primary'
									id='submit_button'
									style={{ width: '100%' }}
									type='submit'
									variant='contained'
								>
									Reset Password
								</Button>
							</Grid>
							<Grid item xs={12} sm={4} style={{ alignSelf: 'center' }}>
								<Link to='/'>Login</Link>
							</Grid>
						</Grid>
					</form>
				)}
				{success && (
					<Typography>
						If an account with the email address exists, the link to reset the
						password will be sent to that email address.
					</Typography>
				)}
			</Paper>
		</div>
	)
}

export default PageForgotPassword
