import React from 'react'

import Header from '../layout/Header'

const PageHome = () => {
	return (
		<div>
			<Header />
			Homepage
		</div>
	)
}

export default PageHome
