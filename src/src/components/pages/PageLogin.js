import React, { useState } from 'react'
import {
	Button,
	Grid,
	makeStyles,
	Paper,
	TextField,
	Typography,
} from '@material-ui/core'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import logo from '../../logo.svg'
import validate from '../../actions/validate'
import { authUpdate } from '../../actions/authActions'
import { useStyles } from '../../theme'

const formConfiguration = {
	password: {
		presence: true,
	},
	username: {
		alphanumeric: true,
		presence: true,
	},
}

const useStylesLocal = makeStyles(theme => ({
	inputField: {
		width: '100%',
	},
	loginBox: {
		margin: theme.spacing(3),
		maxWidth: '360px',
		padding: theme.spacing(3),
	},
	logo: {
		height: '60px',
		marginBottom: '24px',
	},
}))

const PageLogin = ({ authUpdate }) => {
	const [formData, setFormData] = useState({
		password: { error: '', value: '' },
		username: { error: '', value: '' },
	})

	const classes = useStyles()
	const classesLocal = useStylesLocal()

	const inputHandler = ({ target: { name, value } }) => {
		const copy = { ...formData }
		copy[name].value = value
		copy[name].error = validate(name, copy, formConfiguration[name])
		setFormData(copy)
	}

	const submitResetEmail = event => {
		event.preventDefault()

		const copy = { ...formData }
		const errors = []

		for (const key in formData) {
			copy[key].error = validate(key, copy, formConfiguration[key])
			if (copy[key].error) errors.push(copy[key].error)
		}

		setFormData(copy)

		if (errors.length === 0)
			authUpdate({
				isAuth: true,
				token: 'asdekaofnasuivyaosigvbasiubvasdv',
				username: copy.username.value,
			})
	}

	return (
		<div className={`${classes.centerizer} ${classes.root}`}>
			<Paper className={classesLocal.loginBox} elevation={3}>
				<img alt='logo' className={classesLocal.logo} src={logo} />
				<form autoComplete='off' noValidate onSubmit={submitResetEmail}>
					<Grid container spacing={3}>
						<Grid item xs={12}>
							<TextField
								autoFocus={true}
								className={classesLocal.inputField}
								label='Username'
								name='username'
								onChange={inputHandler}
								value={formData.username.value}
								variant='outlined'
							/>
							<Typography className={classes.formErrorMessage}>
								{formData.username.error}
							</Typography>
						</Grid>
						<Grid item xs={12}>
							<TextField
								className={classesLocal.inputField}
								label='Password'
								name='password'
								onChange={inputHandler}
								type='password'
								value={formData.password.value}
								variant='outlined'
							/>
							<Typography className={classes.formErrorMessage}>
								{formData.password.error}
							</Typography>
						</Grid>
						<Grid item xs={12}>
							<Button
								color='primary'
								style={{ width: '100%' }}
								type='submit'
								variant='contained'
							>
								Login
							</Button>
						</Grid>
						<Grid item xs={6} style={{ alignSelf: 'center' }}>
							<Link to='/create-account'>Create Account</Link>
						</Grid>
						<Grid item xs={6} style={{ alignSelf: 'center' }}>
							<Link to='/forgot-password'>Forgot Password</Link>
						</Grid>
					</Grid>
				</form>
			</Paper>
		</div>
	)
}

export default connect(null, { authUpdate })(PageLogin)
