import React, { useState } from 'react'
import {
	Button,
	Grid,
	makeStyles,
	Paper,
	TextField,
	Typography,
} from '@material-ui/core'
import { Link } from 'react-router-dom'

import logo from '../../logo.svg'
import validate from '../../actions/validate'
import { useStyles } from '../../theme'

const formConfiguration = {
	confirm_password: {
		equality: {
			field: 'password',
			message: 'Passwords do not match.',
		},
		presence: true,
	},
	password: {
		format: [
			{
				message: 'Password should contain atleast 8 characters.',
				pattern: /^.{8,}$/,
			},
			{
				message: 'Password should contain an uppercase characters.',
				pattern: /[A-Z]+/,
			},
			{
				message: 'Password should contain a lowercase characters.',
				pattern: /[a-z]+/,
			},
			{
				message: 'Password should contain a number.',
				pattern: /[0-9]+/,
			},
			{
				message:
					'Password should contain one of these special characters @ # $ % ^ & *',
				pattern: /[@#$%^&*]+/,
			},
		],
		presence: true,
	},
	username: {
		alphanumeric: true,
		presence: true,
	},
}

const useStylesLocal = makeStyles(theme => ({
	inputField: {
		width: '100%',
	},
	loginBox: {
		margin: theme.spacing(3),
		maxWidth: '360px',
		padding: theme.spacing(3),
	},
	logo: {
		height: '60px',
		marginBottom: '24px',
	},
}))

const PageCreateAccount = ({ history }) => {
	const [formData, setFormData] = useState({
		confirm_password: { error: '', value: '' },
		password: { error: '', value: '' },
		username: { error: '', value: '' },
	})

	const classes = useStyles()
	const classesLocal = useStylesLocal()

	const inputHandler = ({ target: { name, value } }) => {
		const copy = { ...formData }
		copy[name].value = value
		copy[name].error = validate(name, copy, formConfiguration[name])
		setFormData(copy)
	}

	const submitResetEmail = event => {
		event.preventDefault()

		const copy = { ...formData }
		const errors = []

		for (const key in formData) {
			copy[key].error = validate(key, copy, formConfiguration[key])
			if (copy[key].error) errors.push(copy[key].error)
		}

		setFormData(copy)

		if (errors.length === 0) history.push('/')
	}

	return (
		<div className={`${classes.centerizer} ${classes.root}`}>
			<Paper className={classesLocal.loginBox} elevation={3}>
				<img alt='logo' className={classesLocal.logo} src={logo} />
				<form autoComplete='off' noValidate onSubmit={submitResetEmail}>
					<Grid container spacing={3}>
						<Grid item xs={12}>
							<TextField
								autoFocus={true}
								className={classesLocal.inputField}
								label='Username'
								name='username'
								onChange={inputHandler}
								value={formData.username.value}
								variant='outlined'
							/>
							<Typography className={classes.formErrorMessage}>
								{formData.username.error}
							</Typography>
						</Grid>
						<Grid item xs={12}>
							<TextField
								className={classesLocal.inputField}
								label='Password'
								name='password'
								onChange={inputHandler}
								type='password'
								value={formData.password.value}
								variant='outlined'
							/>
							<Typography className={classes.formErrorMessage}>
								{formData.password.error}
							</Typography>
						</Grid>
						<Grid item xs={12}>
							<TextField
								className={classesLocal.inputField}
								label='Confirm Password'
								name='confirm_password'
								onChange={inputHandler}
								type='password'
								value={formData.confirm_password.value}
								variant='outlined'
							/>
							<Typography className={classes.formErrorMessage}>
								{formData.confirm_password.error}
							</Typography>
						</Grid>
						<Grid item xs={12}>
							<Button
								color='primary'
								style={{ width: '100%' }}
								type='submit'
								variant='contained'
							>
								Create Account
							</Button>
						</Grid>
						<Grid item xs={12} style={{ alignSelf: 'center' }}>
							<Link to='/'>Login</Link>
						</Grid>
					</Grid>
				</form>
			</Paper>
		</div>
	)
}

export default PageCreateAccount
