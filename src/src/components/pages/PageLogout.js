import React, { useEffect } from 'react'
import { Typography } from '@material-ui/core'
import { connect } from 'react-redux'

import { authReset } from '../../actions/authActions'
import { useStyles } from '../../theme'

const PageLogin = ({ authReset, authUpdate }) => {
	const classes = useStyles()

	useEffect(() => {
		authReset()
		// eslint-disable-next-line
	}, [])

	return (
		<div className={`${classes.centerizer} ${classes.root}`}>
			<Typography>Logging out...</Typography>
		</div>
	)
}

const mapStateToProps = state => ({ authState: state.auth })

export default connect(mapStateToProps, { authReset })(PageLogin)
