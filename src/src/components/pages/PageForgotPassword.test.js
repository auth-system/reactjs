import React from 'react'
import { mount } from 'enzyme'
import { BrowserRouter as Router } from 'react-router-dom'

import PageForgotPassword from './PageForgotPassword'

const component = mount(
	<Router>
		<PageForgotPassword />
	</Router>
)

describe('Page: Forgot Password', () => {
	it('Default', () => {
		expect(component).toMatchSnapshot()
	})

	it('Invalid email address', () => {
		const event = { target: { name: 'email_address', value: 'adaw' } }
		component.find('input[name="email_address"]').simulate('change', event)
		expect(component).toMatchSnapshot()
	})

	it('Empty', () => {
		const event = { target: { name: 'email_address', value: '' } }
		component.find('input[name="email_address"]').simulate('change', event)
		expect(component).toMatchSnapshot()
	})

	it('Submit with error', () => {
		component.find('form').simulate('submit')
		expect(component).toMatchSnapshot()
	})

	it('Valid email address', () => {
		const event = {
			target: { name: 'email_address', value: 'jagannathbhatjb@gmail.com' },
		}
		component.find('input[name="email_address"]').simulate('change', event)
		expect(component).toMatchSnapshot()
	})

	it('Submit', () => {
		component.find('form').simulate('submit')
		expect(component).toMatchSnapshot()
	})
})
