import React from 'react'
import { render } from 'enzyme'
import { BrowserRouter as Router } from 'react-router-dom'

import Header from './Header'

test('Header component', () => {
	const component = render(
		<Router>
			<Header />
		</Router>
	)

	expect(component).toMatchSnapshot()
})
