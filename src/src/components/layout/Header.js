import React from 'react'
import { makeStyles, Paper } from '@material-ui/core'
import { Link } from 'react-router-dom'

import { useStyles } from '../../theme'

const useStylesLocal = makeStyles(theme => ({
	header: {
		padding: theme.spacing(2),
		textAlign: 'right',
		width: '100%',
	},
}))

const Header = () => {
	const classes = useStyles()
	const classesLocal = useStylesLocal()

	return (
		<Paper className={classesLocal.header} elevation={3}>
			<Link className={classes.link} to='/logout'>
				Logout
			</Link>
		</Paper>
	)
}

export default Header
