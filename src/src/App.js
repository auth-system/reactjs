// import React, { useState } from 'react'
import React from 'react'
import {
	// createMuiTheme,
	CssBaseline,
	// ThemeProvider,
} from '@material-ui/core'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'

import './App.css'
import store from './store'
import Pages from './Pages'
// import { getMode, setMode } from './actions/darkModeActions'

function App() {
	// const [darkMode, setDarkMode] = useState(getMode())

	// const toggleDarkMode = () => {
	// 	setDarkMode(!darkMode)
	// 	setMode(!darkMode)
	// }

	// const theme = React
	// 	.useMemo
	// 	// () =>
	// 	// 	createMuiTheme({
	// 	// 		palette: {
	// 	// 			type: darkMode ? 'dark' : 'light',
	// 	// 		},
	// 	// 	}),
	// 	// [darkMode]
	// 	()

	return (
		<Provider store={store}>
			{/* <ThemeProvider theme={theme}> */}
			<CssBaseline />
			<Router>
				<Pages />
			</Router>
			{/* </ThemeProvider> */}
		</Provider>
	)
}

export default App
