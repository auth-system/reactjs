import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'

import PageCreateAccount from './components/pages/PageCreateAccount'
import PageForgotPassword from './components/pages/PageForgotPassword'
import PageHome from './components/pages/PageHome'
import PageLogin from './components/pages/PageLogin'
import PageLogout from './components/pages/PageLogout'

const Pages = ({ authState }) => {
	if (authState.isAuth)
		return (
			<Switch>
				<Route component={PageHome} exact path='/' />
				<Route component={PageLogout} exact path='/logout' />
			</Switch>
		)
	else
		return (
			<Switch>
				<Route component={PageCreateAccount} exact path='/create-account' />
				<Route component={PageForgotPassword} exact path='/forgot-password' />
				<Route component={PageLogin} exact path='/' />
				<Route>
					<Redirect to='/' />
				</Route>
			</Switch>
		)
}

const mapStateToProps = state => ({ authState: state.auth })

export default connect(mapStateToProps)(Pages)
